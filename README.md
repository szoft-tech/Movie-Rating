# Movie Rating
Build status: [![build status](https://gitlab.com/szoft-tech/Movie-Rating/badges/release/build.svg)](https://gitlab.com/szoft-tech/Movie-Rating/commits/release)

Documents:    
[Software requirement specification](/../wikis/Software-Requirement-Specification)    
[Software design specification](/../wikis/Software-Design-Specification)    
[User documentation](/../wikis/User-documentation)    

## Install

You have to install to run the application:
* Docker engine. The version should be at least 1.13.1.

Pull the latest image from the registry with the following commands.    
`docker login registry.gitlab.com`    
`docker pull registry.gitlab.com/szoft-tech/movie-rating:latest`    

## Usage

Start the docker container from the pulled image.    
`docker run --name [container name] -d -p [port on localhost]:80 registry.gitlab.com/szoft-tech/movie-rating:latest`

Example parameters:    
[port on localhost] = 8088    
[container name] = movie-rating

The site is now available on localhost:[given port]. 

To stop the service, use `docker stop [container name]`. 

## Saving the actual state

Export the database:

- Open SQL Server Management Studio
- Connect to the current database server.

`Server name: 157.181.161.108,1401` <br>
`Login: MovieDBUser` <br>
`Password: Copy the password from the Keepass`
- Right click on the database
- Tasks --> Generate scripts --> Advanced
- There you can enable "Type of data to script" as Schema and data, then generate the script. So that your script file will be populated with your data in table.

## Create a new database server

Create a new volume <br>
`docker volume create mssql_volume` <br>

Start the docker container from the pulled image. <br>
`docker run -e 'ACCEPT_EULA=Y' -e 'MSSQL_SA_PASSWORD=<your strong password> -p 1401:1433 --name mssql -v mssql_volume:/var/opt/mssql -d microsoft/mssql-server-linux:2017-latest`


## Reloading a previously saved state

Import the database:

- Open SQL Server Management Studio
- Connect to the current database server.

`Server name: 157.181.161.108,1401` <br>
`Login: MovieDBUser` <br>
`Password: Copy the password from the Keepass`
- Open the previously exported database and execute the script. 
