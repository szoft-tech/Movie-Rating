﻿using System;
using System.Collections.Generic;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class Group
    {
        public Group()
        {
            Usergroup = new HashSet<Usergroup>();
        }

        public int GroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }

        public ICollection<Usergroup> Usergroup { get; set; }
    }
}
