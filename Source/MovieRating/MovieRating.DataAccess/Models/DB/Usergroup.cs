﻿using System;
using System.Collections.Generic;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class Usergroup
    {
        public int UsergroupId { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
        public int Level { get; set; }
        public DateTime? JoinedDate { get; set; }

        public Group Group { get; set; }
        public User User { get; set; }
    }
}
