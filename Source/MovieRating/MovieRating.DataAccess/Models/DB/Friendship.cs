﻿using System;
using System.Collections.Generic;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class Friendship
    {
        public int FriendshipId { get; set; }
        public int UserId { get; set; }
        public int FriendId { get; set; }
        public int Status { get; set; }
        public DateTime? FriendedDate { get; set; }

        public User FriendUser { get; set; }
        public User User { get; set; }
    }
}
