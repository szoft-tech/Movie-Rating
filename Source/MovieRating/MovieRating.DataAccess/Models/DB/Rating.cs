﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class Rating
    {
        public int RatingId { get; set; }
        public int UserId { get; set; }
        public int MovieId { get; set; }
        [Range(1, 10, ErrorMessage = "Your rate must be between 1 and 10")]
        public int Score { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public DateTime? RatedDate { get; set; }

        public Movie Movie { get; set; }
        public User User { get; set; }
    }
}
