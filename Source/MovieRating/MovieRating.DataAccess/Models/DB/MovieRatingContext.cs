﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class MovieRatingContext : DbContext
    {
        public virtual DbSet<Friendship> Friendship { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<Movie> Movie { get; set; }
        public virtual DbSet<MovieList> MovieList { get; set; }
        public virtual DbSet<MovieType> MovieType { get; set; }
        public virtual DbSet<Rating> Rating { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Usergroup> Usergroup { get; set; }
        public virtual DbSet<Userlist> Userlist { get; set; }

        public MovieRatingContext(DbContextOptions<MovieRatingContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Friendship>(entity =>
            {
                entity.ToTable("friendship");

                entity.HasIndex(e => new { e.UserId, e.FriendId })
                    .HasName("uk_friendship")
                    .IsUnique();

                entity.Property(e => e.FriendshipId)
                    .HasColumnName("friendship_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.FriendId).HasColumnName("friend_id");

                entity.Property(e => e.FriendedDate)
                    .HasColumnName("friended_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.FriendUser)
                    .WithMany(p => p.OtherUserRequestedFriendships)
                    .HasForeignKey(d => d.FriendId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_friendship_user_2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.MyRequestedFriendships)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_friendship_user_1");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("group");

                entity.Property(e => e.GroupId).HasColumnName("group_id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.ToTable("movie");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.AddedDate)
                    .HasColumnName("added_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ImdbId)
                    .HasColumnName("imdb_id")
                    .HasMaxLength(20);

                entity.Property(e => e.MovieApiId).HasColumnName("movie_api_id");

                entity.Property(e => e.MovieTypeId).HasColumnName("movie_type_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Year).HasColumnName("year");

                entity.HasOne(d => d.MovieType)
                    .WithMany(p => p.Movie)
                    .HasForeignKey(d => d.MovieTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_movie_movie_type");
            });

            modelBuilder.Entity<MovieList>(entity =>
            {
                entity.HasKey(e => e.ListElementId);

                entity.ToTable("movie_list");

                entity.Property(e => e.ListElementId).HasColumnName("list_element_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.UserlistId).HasColumnName("userlist_id");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieList)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_list_movie");

                entity.HasOne(d => d.Userlist)
                    .WithMany(p => p.MovieList)
                    .HasForeignKey(d => d.UserlistId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_list_userlist");
            });

            modelBuilder.Entity<MovieType>(entity =>
            {
                entity.ToTable("movie_type");

                entity.Property(e => e.MovieTypeId).HasColumnName("movie_type_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.ToTable("rating");

                entity.Property(e => e.RatingId).HasColumnName("rating_id");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(500);

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.RatedDate)
                    .HasColumnName("rated_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Score)
                    .IsRequired()
                    .HasColumnName("score");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.Rating)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_rating_movie");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Rating)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_rating_user");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50);

                entity.Property(e => e.JoinedDate)
                    .HasColumnName("joined_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(30);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(128);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Usergroup>(entity =>
            {
                entity.ToTable("usergroup");

                entity.Property(e => e.UsergroupId).HasColumnName("usergroup_id");

                entity.Property(e => e.GroupId).HasColumnName("group_id");

                entity.Property(e => e.JoinedDate)
                    .HasColumnName("joined_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Level).HasColumnName("level");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Usergroup)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_usergroup_group");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Usergroup)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_usergroup_user");
            });

            modelBuilder.Entity<Userlist>(entity =>
            {
                entity.ToTable("userlist");

                entity.Property(e => e.UserlistId).HasColumnName("userlist_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Userlist)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_user_id");

                entity.Property(e => e.Public).HasColumnName("list_public");
            });
        }
    }
}
