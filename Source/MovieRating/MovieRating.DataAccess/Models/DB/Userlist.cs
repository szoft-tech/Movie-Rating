﻿using System;
using System.Collections.Generic;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class Userlist
    {
        public Userlist()
        {
            MovieList = new HashSet<MovieList>();
        }

        public int UserlistId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public Boolean Public { get; set; }

        public User User { get; set; }
        public ICollection<MovieList> MovieList { get; set; }
    }
}
