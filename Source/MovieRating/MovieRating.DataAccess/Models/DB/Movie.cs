﻿using System;
using System.Collections.Generic;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class Movie
    {
        public Movie()
        {
            MovieList = new HashSet<MovieList>();
            Rating = new HashSet<Rating>();
        }

        public int MovieId { get; set; }
        public int MovieApiId { get; set; }
        public int MovieTypeId { get; set; }
        public string Name { get; set; }
        public short Year { get; set; }
        public string ImdbId { get; set; }
        public DateTime? AddedDate { get; set; }

        public MovieType MovieType { get; set; }
        public ICollection<MovieList> MovieList { get; set; }
        public ICollection<Rating> Rating { get; set; }
    }
}
