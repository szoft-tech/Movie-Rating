﻿using System;
using System.Collections.Generic;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class MovieType
    {
        public MovieType()
        {
            Movie = new HashSet<Movie>();
        }

        public int MovieTypeId { get; set; }
        public string Name { get; set; }

        public ICollection<Movie> Movie { get; set; }
    }
}
