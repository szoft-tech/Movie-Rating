﻿using System;
using System.Collections.Generic;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class User
    {
        public User()
        {
            MyRequestedFriendships = new HashSet<Friendship>();
            OtherUserRequestedFriendships = new HashSet<Friendship>();
            Rating = new HashSet<Rating>();
            Usergroup = new HashSet<Usergroup>();
            Userlist = new HashSet<Userlist>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime? JoinedDate { get; set; }

        public ICollection<Friendship> MyRequestedFriendships { get; set; }
        public ICollection<Friendship> OtherUserRequestedFriendships { get; set; }
        public ICollection<Rating> Rating { get; set; }
        public ICollection<Usergroup> Usergroup { get; set; }
        public ICollection<Userlist> Userlist { get; set; }
    }
}
