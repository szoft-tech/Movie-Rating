﻿using System;
using System.Collections.Generic;

namespace MovieRating.DataAccess.Models.DB
{
    public partial class MovieList
    {
        public int ListElementId { get; set; }
        public int UserlistId { get; set; }
        public int MovieId { get; set; }

        public Movie Movie { get; set; }
        public Userlist Userlist { get; set; }
    }
}
