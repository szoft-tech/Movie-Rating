using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieRating.DataAccess.Models.DB;
using MovieRating.Web;
using MovieRating.Web.Controllers;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieRating.DataAccess.Controllers;

namespace MovieRating.Business.Tests
{
    public class MovieRatingContextMock : MovieRatingContext
    {
        public MovieRatingContextMock() : base(new DbContextOptions<MovieRatingContext>()) { }
    }

    [TestClass]
    public class MoviesControllerTest
    {
        private Mock<IOptions<APIConfiguration>> apiConfigMock;
        private Mock<MovieRatingContextMock> contextMock;

        private MoviesController controller;

        [TestInitialize]
        public void Init()
        {
            this.contextMock = new Mock<MovieRatingContextMock>(MockBehavior.Loose);

            controller = new MoviesController(contextMock.Object);
        }

        [TestMethod]
        public async Task TestCreateAsync()
        {
            Movie testData = new Movie();   //...

            this.contextMock
                .Setup(context => context.Add(testData))
                .Verifiable("Received movie needs to be added.");
            
            await controller.Create(testData);

            this.contextMock.VerifyAll();
        }
    }
}
