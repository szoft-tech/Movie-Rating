﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieRating.DataAccess.Models.DB;
using MovieRating.Web.Controllers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;

namespace MovieRating.Web.Tests.Controllers
{
    public class MovieRatingContextMock : MovieRatingContext
    {
        public MovieRatingContextMock() : base(new DbContextOptions<MovieRatingContext>()) { }
    }

    public class FriendsControllerUnderTest : FriendsController
    {
        public FriendsControllerUnderTest(MovieRatingContext context) : base(context) { }

        protected override async Task<User> GetCurrentUser()
        {
            var currentUser = new User()
            {
                Username = "test",
                UserId = 1
            };

            return currentUser;
        }
    }

    [TestClass]
    public class FriendsControllerTest : Controller
    {
        private Mock<MovieRatingContextMock> contextMock;
        private FriendsControllerUnderTest controller;

        [TestInitialize]
        public void Init()
        {
            this.contextMock = new Mock<MovieRatingContextMock>(MockBehavior.Loose);

            controller = new FriendsControllerUnderTest(contextMock.Object);
        }

        [TestMethod]
        public async Task TestSendRequestAsync()
        {
            int testId = 2;

            var friendships = new List<Friendship>().AsQueryable();
            var friendshipsMock = new Mock<DbSet<Friendship>>();
            friendshipsMock.As<IQueryable<Friendship>>().Setup(m => m.Provider).Returns(friendships.Provider);
            friendshipsMock.As<IQueryable<Friendship>>().Setup(m => m.Expression).Returns(friendships.Expression);
            friendshipsMock.As<IQueryable<Friendship>>().Setup(m => m.ElementType).Returns(friendships.ElementType);
            friendshipsMock.As<IQueryable<Friendship>>().Setup(m => m.GetEnumerator()).Returns(friendships.GetEnumerator());
            friendshipsMock
                .Setup(context => context.Add(It.IsAny<Friendship>()));

            this.contextMock
                .Setup(context => context.Friendship)
                .Returns(friendshipsMock.Object);
            
            await controller.SendRequest(testId);

            friendshipsMock.Verify(context =>
                context.Add(Match.Create<Friendship>(f => f.FriendId == testId && f.Status == 0)));
        }
    }
}
