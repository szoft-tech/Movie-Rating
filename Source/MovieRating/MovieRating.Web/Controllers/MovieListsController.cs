﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MovieRating.DataAccess.Models.DB;
using Microsoft.AspNetCore.Authorization;

namespace MovieRating.DataAccess.Controllers
{
    [Authorize]
    public class MovieListsController : Controller
    {
        private readonly MovieRatingContext _context;

        public MovieListsController(MovieRatingContext context)
        {
            _context = context;
        }


        // GET: MovieLists
        public async Task<IActionResult> Index()
        {
            return View(_context.Userlist.Where(u => u.UserId == GetUserId()).ToList());
        }

        public IActionResult Create()
        {
            ViewData["UserlistId"] = new SelectList(_context.Userlist, "UserlistId", "Name", "Public");
            return View();
        }

        // POST: Group/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserlistId,Name")] Userlist list, string Visibility)
        {
            list.Public = (Visibility == "Public");

            var isListNameInUse = _context.Userlist.Any(l => l.UserId == GetUserId() && l.Name == list.Name);
            if (isListNameInUse)
            {
                ModelState.AddModelError("Name", "The list name must be unique.");
            }
            
            if (ModelState.IsValid)
            {
                list.UserId = GetUserId();
                _context.Add(list);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(list);
        }

        public IActionResult SelectLists(int movieId)
        {
            ViewData["MovieId"] = movieId;
            return View(_context.Userlist.Where(u => u.UserId == GetUserId()).ToList());
        }


        public async Task<IActionResult> SaveSelectedLists(int listId, int movieId)
        {
            if (ModelState.IsValid)
            {
                if (!CheckUser(listId))
                {
                    return RedirectToAction(nameof(Index));
                }
                if (!_context.MovieList.Where(m => m.MovieId == movieId && m.UserlistId == listId).Any())
                {
                    MovieList ml2 = new MovieList();
                    ml2.MovieId = movieId;
                    ml2.UserlistId = listId;
                    _context.Add(ml2);

                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "Movies");
                }

            }
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            if (!CheckUser((int)id))
            {
                return RedirectToAction(nameof(Index));
            }
            var list = await _context.Userlist.SingleOrDefaultAsync(m => m.UserlistId == id);
            List<MovieList> movieList = _context.MovieList.Where(m => m.UserlistId == id).ToList();
            foreach (MovieList ml in movieList)
            {
                _context.MovieList.Remove(ml);
            }

            _context.Userlist.Remove(list);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Details(int? listId, int? userId)
        {
            ViewData["CurrentUser"] = "";
            if (listId == null)
            {
                return NotFound();
            }
            if (!CheckUser((int)listId) && !IsFriendUser((int)listId))
            {
                return RedirectToAction(nameof(Index));
            }
            if (userId != null && GetUserId() != _context.Userlist.First(u => u.UserlistId == listId).UserId)
            {
                ViewData["CurrentUser"] = "other";
            }
            var list = await _context.Userlist.SingleOrDefaultAsync(m => m.UserlistId == listId);
            ViewData["ListName"] = _context.Userlist.Find(listId).Name;
            ViewData["ListId"] = listId;

            List<MovieList> movieLists = _context.MovieList.Include(m => m.Movie).Where(m => m.Userlist.UserlistId == listId).ToList();
            return View(movieLists);
        }

        public async Task<IActionResult> Remove(int? listId, int? movieId)
        {
            if (listId == null || movieId == null)
            {
                return NotFound();
            }
            if (!CheckUser((int)listId))
            {
                return RedirectToAction(nameof(Index));
            }
            var movie = await _context.MovieList
                            .SingleOrDefaultAsync(m => m.UserlistId == listId && m.ListElementId == movieId);
            _context.MovieList.Remove(movie);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", new { listId = listId });
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            if (!CheckUser((int)id))
            {
                return RedirectToAction(nameof(Index));
            }
            var list = await _context.Userlist.SingleOrDefaultAsync(m => m.UserlistId == id);
            if (list == null)
            {
                return NotFound();
            }

            ViewData["UserlistId"] = new SelectList(_context.Userlist, "UserlistId", "Name", "Public");
            return View(list);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("UserlistId,Name")] Userlist list, string Visibility)
        {
            list.Public = (Visibility == "Public");
            if (ModelState.IsValid)
            {
                try
                {
                    list.UserId = GetUserId();

                    _context.Update(list);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserlstId"] = new SelectList(_context.MovieType, "UserlistId", "Name", "Public");
            return View(list);
        }

        private int GetUserId()
        {
            String userName = this.User.Identity.Name;
            int userId = _context.User.ToList().Find(r => r.Username.Equals(userName)).UserId;
            return userId;
        }

        private bool CheckUser(int id)
        {
            int userId = GetUserId();
            return _context.Userlist.Include(u => u.User).Any(u => u.UserlistId == id && u.UserId == userId);
        }

        private bool IsFriendUser(int id)
        {
            int userId = GetUserId();
            List<Friendship> friends = _context.Friendship.Include(f => f.FriendUser)
                                            .Include(f => f.User).Where(f => (f.UserId == userId || f.FriendId == userId) && f.Status == 1).ToList();

            foreach (Friendship friend in friends)
            {
                if (_context.Userlist.Any(u => (u.UserId == friend.FriendId || u.UserId == friend.UserId) && u.UserlistId == id && u.Public))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
