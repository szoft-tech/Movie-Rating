﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using TMDbLib.Client;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using MovieRating.DataAccess.Models.DB;
using TMDbLib.Objects.Movies;

namespace MovieRating.Web.Controllers
{
    [Authorize]
    public class TMDBController : Controller
    {
        private readonly APIConfiguration _apiconfig;
        private MovieRatingContext _context;
        private TMDbClient _client;
        private Boolean _inited;

        public TMDBController(MovieRatingContext context, IOptions<APIConfiguration> apiconfig)
        {
            _apiconfig = apiconfig.Value;
            _context = context;
            try
            {
                _client = new TMDbClient(_apiconfig.Key);
                _inited = true;
            }
            catch
            {
                _inited = false;
            }
        }

        // GET: TMDB/Index
        public async Task<IActionResult> Index()
        {
            var Movies = await _client.GetMoviePopularListAsync();

            return View("Search", Movies.Results);
        }

        // GET: TMDB/Search/keywords
        public async Task<IActionResult> Search(String keywords)
        {
            var Movies = await _client.SearchMovieAsync(keywords);

            ViewData["keywords"] = keywords;

            return View(Movies.Results);
        }

        [HttpGet]
        // GET : TMDB/Details/id
        public async Task<IActionResult> Details(int id)
        {
            var Movie = await _client.GetMovieAsync(id, MovieMethods.Credits);

            return View(Movie);
        }

        // POST : TMDB/Rate/id
        [HttpPost]
        public IActionResult Rate(TMDbLib.Objects.Movies.Movie model)
        {
            var savedMovie = GetMovieIfNotExist(model);

            return RedirectToAction("Rate", "Movies", new { id = savedMovie.MovieId });
        }

        // POST : TMDB/AddList/id
        [HttpPost]
        public IActionResult AddList(TMDbLib.Objects.Movies.Movie model)
        {

            var savedMovie = GetMovieIfNotExist(model);

            return RedirectToAction("SelectLists", "MovieLists", new { movieId = savedMovie.MovieId });
        }

        /// <summary>
        /// Insert the movie to the database if it is not exist
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private DataAccess.Models.DB.Movie GetMovieIfNotExist(TMDbLib.Objects.Movies.Movie model)
        {
            var savedMovie = _context.Movie.FirstOrDefault(m => m.MovieApiId == model.Id);

            if (savedMovie == null)
            {
                savedMovie = new DataAccess.Models.DB.Movie
                {
                    ImdbId = model.ImdbId,
                    Name = model.Title,
                    Year = (short)model.ReleaseDate.Value.Year,
                    MovieApiId = model.Id,
                    MovieTypeId = 1
                };

                _context.Add(savedMovie);
                _context.SaveChanges();
            }

            return savedMovie;
        }
    }
}
