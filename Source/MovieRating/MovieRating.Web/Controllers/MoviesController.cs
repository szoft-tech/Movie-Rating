﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MovieRating.DataAccess.Models.DB;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using MovieRating.Web;

namespace MovieRating.DataAccess.Controllers
{
    [Authorize]
    public class MoviesController : Controller
    {
        private readonly MovieRatingContext _context;

        public MoviesController(MovieRatingContext context)
        {
            _context = context;
        }

        // GET: Movies
        public async Task<IActionResult> Index()
        {
            var Movies = _context.Movie.ToList();
            foreach (var movie in Movies)
            {
                movie.Rating = _context.Rating.Where(r => r.MovieId == movie.MovieId).ToList();
            }
            var CurrentUser = await getCurrentUser();
            var FilteredMovies = Movies.Where(m => !m.Rating.Any(r => r.UserId == CurrentUser.UserId));
            return View(FilteredMovies.OrderByDescending(m => m.Rating.DefaultIfEmpty(new Rating { Score = 0 }).Average(r => r.Score)).Take(15).ToList());
        }

        // GET: Movies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movie
                .Include(m => m.MovieType)
                .SingleOrDefaultAsync(m => m.MovieId == id);
            if (movie == null)
            {
                return NotFound();
            }

            movie.Rating = _context.Rating.Where(r => r.MovieId == movie.MovieId).ToList();

            foreach (var rating in movie.Rating)
            {
                rating.User = _context.User.Where(u => u.UserId == rating.UserId).FirstOrDefault();
            }

            return View(movie);
        }

        // GET: Movies/Create
        public IActionResult Create()
        {
            ViewData["MovieTypeId"] = new SelectList(_context.MovieType, "MovieTypeId", "Name");
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MovieId,MovieApiId,MovieTypeId,Name,Year,ImdbId,AddedDate")] Movie movie)
        {
            if (ModelState.IsValid)
            {
                _context.Add(movie);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieTypeId"] = new SelectList(_context.MovieType, "MovieTypeId", "Name", movie.MovieTypeId);
            return View(movie);
        }

        // GET: Movies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movie.SingleOrDefaultAsync(m => m.MovieId == id);
            if (movie == null)
            {
                return NotFound();
            }
            ViewData["MovieTypeId"] = new SelectList(_context.MovieType, "MovieTypeId", "Name", movie.MovieTypeId);
            return View(movie);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MovieId,MovieApiId,MovieTypeId,Name,Year,ImdbId,AddedDate")] Movie movie)
        {
            if (id != movie.MovieId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(movie);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieExists(movie.MovieId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieTypeId"] = new SelectList(_context.MovieType, "MovieTypeId", "Name", movie.MovieTypeId);
            return View(movie);
        }

        // GET: Movies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movie
                .Include(m => m.MovieType)
                .SingleOrDefaultAsync(m => m.MovieId == id);
            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var movie = await _context.Movie.SingleOrDefaultAsync(m => m.MovieId == id);
            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.MovieId == id);
        }

        
        public async Task<IActionResult> AddToList(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            List<Movie> movies = _context.Movie.Where(m => m.MovieId == id).ToList();
            return RedirectToAction("SelectLists", "MovieLists", new { movieId = id });
        }

        public async Task<IActionResult> Rate(int id)
        {
            var rate = await GetCurrentRating(id);

            return View(rate ?? new Rating { MovieId = id });
        }

        [HttpPost]
        public async Task<IActionResult> Rate(Rating model)
        {
            var rate = await GetCurrentRating(model.MovieId);

            if (rate == null)
            {
                var currentMovie = _context.Movie.Where(m => m.MovieId == model.MovieId).First();
                var currentUser = await getCurrentUser();

                rate = model;
                rate.Movie = currentMovie;
                rate.User = currentUser;
                _context.Add(rate);
            }

            rate.Score = model.Score;
            rate.Description = model.Description;

            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        private async Task<Rating> GetCurrentRating(int movieId)
        {
            var currentMovie = _context.Movie.Where(m => m.MovieId == movieId).First();
            var currentUser = await getCurrentUser();

            var rate = _context.Rating.FirstOrDefault(r => r.MovieId == currentMovie.MovieId && r.UserId == currentUser.UserId);
            return rate;
        }

        private async Task<User> getCurrentUser()
        {
            User currentUser = null;
            foreach (Claim claim in User.Claims.ToList())
            {
                if (claim.Type == ClaimTypes.Name)
                {
                    currentUser = await _context.User.FirstOrDefaultAsync(r => r.Username.Equals(claim.Value));
                }
            }
            return currentUser;
        }
    }
}
