﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieRating.DataAccess.Models.DB;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Security.Claims;

namespace MovieRating.Web.Controllers
{
   [Authorize]
    public class GroupsController : Controller
    {
        private readonly MovieRatingContext _context;

        public GroupsController(MovieRatingContext context)
        {
            _context = context;
        }

        // GET: Groups
        public async Task<IActionResult> Index()
        {
            var currentUser = await GetCurrentUser();
            var UserGroups = _context.Usergroup.Where(ug => ug.UserId == currentUser.UserId);

            var PendingUserGroups = UserGroups.Where(ug => ug.Level == 0);
            var PendingGroups = _context.Group.Where(g => PendingUserGroups.Any(ug => ug.GroupId == g.GroupId));

            var MemberUserGroups = UserGroups.Where(ug => ug.Level == 1);
            var MemberGroups = _context.Group.Where(g => MemberUserGroups.Any(ug => ug.GroupId == g.GroupId));
            
            var OwnedUserGroups = UserGroups.Where(ug => ug.Level == 2);
            var OwnedGroups = _context.Group.Where(g => OwnedUserGroups.Any(ug => ug.GroupId == g.GroupId));

            return View(Tuple.Create<IEnumerable<Group>, IEnumerable<Group>, IEnumerable<Group>>(PendingGroups.ToList(), OwnedGroups.ToList(), MemberGroups.ToList()));
        }

        // GET: Groups/Create
        public async Task<IActionResult> Create()
        {
            return View();
        }

        // POST: Group/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Description")] Group group)
        {
            if (ModelState.IsValid)
            {
                var currentUser = await GetCurrentUser();

                _context.Update(group);
                await _context.SaveChangesAsync();

                var UserGroup = new Usergroup
                {
                    GroupId = group.GroupId,
                    UserId = currentUser.UserId,
                    Level = 2
                };
                _context.Update(UserGroup);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(group);
        }

        [HttpGet]
        // GET: Groups/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var currentUser = await GetCurrentUser();
            var UserGroup = _context.Usergroup.Where(u => u.GroupId == id).Where(u => u.UserId == currentUser.UserId).FirstOrDefault();

            if (UserGroup.Level == 2) // Only admin can leave
            {
                _context.Usergroup.Remove(UserGroup);

                var Members = _context.Usergroup.Where(u => u.GroupId == id);
                foreach (var Member in Members)
                {
                    _context.Remove(Member);
                }

                var Group = _context.Group.Where(g => g.GroupId == id).FirstOrDefault();
                _context.Remove(Group);

                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        // GET: Groups/InviteIndex/5
        [HttpGet]
        public async Task<IActionResult> Invite(int id)
        {
            var currentUser = await GetCurrentUser();

            var UserGroup = _context.Usergroup.Where(ug => ug.UserId == currentUser.UserId && ug.GroupId == id).FirstOrDefault();
            if (UserGroup == null) // Not the member of such a group
            {
                return RedirectToAction(nameof(Index));
            }

            var Friendships = _context.Friendship.Where(f => f.UserId == currentUser.UserId || f.FriendId == currentUser.UserId);
            var Friends = _context.User.Where(u => u.UserId != currentUser.UserId && Friendships.Any(f => f.UserId == u.UserId || f.FriendId == u.UserId));

            var GroupUsers = _context.Usergroup.Where(ug => ug.GroupId == id);

            var Group = _context.Group.Where(g => g.GroupId == id).FirstOrDefault();
            ViewData["Group"] = Group;

            return View(Friends.Where(u => !GroupUsers.Any(ug => ug.UserId == u.UserId)).ToList());
        }

        // GET: Groups/Invite/5
        [HttpPost]
        public async Task<IActionResult> Invite(int GroupId, int UserId)
        {
            var currentUser = await GetCurrentUser();

            var UserGroup = _context.Usergroup.Where(ug => ug.UserId == currentUser.UserId && ug.GroupId == GroupId).FirstOrDefault();
            if (UserGroup == null) // Not the member of such a group
            {
                return NotFound();
            }

            var Friend = _context.Friendship
                .Where(f => (f.UserId == currentUser.UserId && f.FriendId == UserId)
                            || (f.FriendId == currentUser.UserId && f.UserId == UserId)).FirstOrDefault();
            if (Friend == null) // Not friends
            {
                return NotFound();
            }

            var FriendUserGroup = _context.Usergroup.Where(ug => ug.UserId == UserId && ug.GroupId == GroupId).FirstOrDefault();
            if (FriendUserGroup != null) // Already member of such group
            {
                return NotFound();
            }

            _context.Usergroup.Add(new Usergroup()
            {
                GroupId = GroupId,
                UserId = UserId,
                Level = 0
            });
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Invite), new { id = GroupId });
        }

        // GET: Groups/AcceptInvitation/5
        [HttpGet]
        public async Task<IActionResult> AcceptInvitation(int id)
        {
            var currentUser = await GetCurrentUser();
            
            var group = await _context.Group.Include(w => w.Usergroup).SingleOrDefaultAsync(g => g.GroupId == id);
            if (group == null)
            {
                return NotFound();
            }

            var userGroup = group.Usergroup.FirstOrDefault(ug => ug.UserId == currentUser.UserId && ug.Level == 0);
            if (userGroup == null)
            {
                return NotFound();
            }

            userGroup.Level = 1;
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        // GET: Groups/ViewRaingts/Index
        [HttpGet]
        public async Task<IActionResult> ViewRatings(int id)
        {
            var currentUser = await GetCurrentUser();

            var UserGroup = _context.Usergroup.Where(ug => ug.UserId == currentUser.UserId && ug.GroupId == id).FirstOrDefault();
            if (UserGroup == null) // Not the member of such a group
            {
                return RedirectToAction(nameof(Index));
            }

            var UserGroups = _context.Usergroup.Where(ug => ug.GroupId == id && ug.Level != 0);
            var Ratings = _context.Rating.Where(r => r.UserId != currentUser.UserId && UserGroups.Any(ug => ug.UserId == r.UserId));
            var UserRatings = _context.Rating.Where(r => r.UserId == currentUser.UserId);
            var Movies = _context.Movie.Where(m => Ratings.Any(r => r.MovieId == m.MovieId) && !UserRatings.Any(r => r.MovieId == m.MovieId));

            foreach (var movie in Movies)
            {
                movie.Rating = Ratings.Where(r => r.MovieId == movie.MovieId).ToList();
            }

            return View(Movies.OrderBy(m => m.Rating.Average(r => r.Score)).Take(15).ToList());
        }

        // GET: Groups/Leave/5
        [HttpGet]
        public async Task<IActionResult> Leave(int id)
        {
            var currentUser = await GetCurrentUser();
            var UserGroup = _context.Usergroup.Where(u => u.GroupId == id).Where(u => u.UserId == currentUser.UserId).FirstOrDefault();

            if (UserGroup.Level == 1) // Only member can leave
            {
                _context.Usergroup.Remove(UserGroup);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        private bool GroupExists(int id)
        {
            return _context.Group.Any(e => e.GroupId == id);
        }

        private async Task<User> GetCurrentUser()
        {
            User currentUser = null;
            foreach (Claim claim in User.Claims.ToList())
            {
                if (claim.Type == ClaimTypes.Name)
                {
                    currentUser = await _context.User.FirstOrDefaultAsync(r => r.Username.Equals(claim.Value));
                }
            }
            return currentUser ?? new User();
        }
    }
}