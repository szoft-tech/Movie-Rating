﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieRating.DataAccess.Models.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication;

namespace MovieRating.Web.Controllers
{
    [Authorize]
    public class ProfilesController : Controller
    {
        private readonly MovieRatingContext _context;

        public ProfilesController(MovieRatingContext context)
        {
            _context = context;
        }

        // GET: Profiles
        public async Task<ActionResult> Index()
        {
            var currentUser = await GetCurrentUser();
            ViewData["currentUser"] = currentUser;
            return View(currentUser);
        }

        // GET: Profiles/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var currentUser = await GetCurrentUser();
            ViewData["currentUser"] = currentUser;

            var areFriends = _context.Friendship.FirstOrDefault(f => 
                (f.UserId == currentUser.UserId && f.FriendId == id && f.Status == 1) ||
                (f.UserId == id && f.FriendId == currentUser.UserId && f.Status == 1)) != null;

            if (currentUser.UserId == id || !areFriends)
            {
                return RedirectToAction(nameof(Index));
            }

            var friendUser = await _context.User.Include(f => f.Userlist).FirstOrDefaultAsync(u => u.UserId == id);

            return View(nameof(Index), friendUser);
        }

        // GET: Profiles/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User.Include(u => u.Usergroup).FirstOrDefaultAsync(u => u.UserId == id);

            if ((await GetCurrentUser()).UserId != id)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(user);
        }

        // POST: Profiles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int? id, [Bind("Username,Name,Email")] User user)
        {
            if (id == null || (await GetCurrentUser()).UserId != id)
            {
                return NotFound();
            }

            try
            {
                var userToModify = await _context.User.FirstOrDefaultAsync(u => u.UserId == id);

                if (user.Username != userToModify.Username && _context.User.Any(u => u.Username == user.Username))
                {
                    ModelState.AddModelError("UserName", "The username is already in use.");
                }

                if (String.IsNullOrWhiteSpace(user.Username))
                {
                    ModelState.AddModelError("UserName", "The username must not be empty.");
                }

                if (ModelState.IsValid)
                {
                    userToModify.Username = user.Username;
                    userToModify.Name = user.Name;
                    userToModify.Email = user.Email;

                    await _context.SaveChangesAsync();

                    if (userToModify.Username.Equals(user.Username))
                    {
                        await HttpContext.SignOutAsync(scheme: "MovieDatabase");
                        await SignInAsync(userToModify.Username);
                    }

                    return RedirectToAction(nameof(Index));
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        private async Task SignInAsync(string username)
        {
            // create claims
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, username),
            };

            // create identity
            ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

            // create principal
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            // sign-in
            await HttpContext.SignInAsync(
                    scheme: "MovieDatabase",
                    principal: principal);
        }

        virtual protected async Task<User> GetCurrentUser()
        {
            User currentUser = null;
            foreach (Claim claim in User.Claims.ToList())
            {
                if (claim.Type == ClaimTypes.Name)
                {
                    currentUser = await _context.User.Include(u => u.Userlist).FirstOrDefaultAsync(u => u.Username.Equals(claim.Value));
                }
            }
            return currentUser ?? new User();
        }
    }
}