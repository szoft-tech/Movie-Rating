﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication;
using System;
using MovieRating.Web.Models;
using MovieRating.DataAccess.Models.DB;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using System.Text;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace MovieRating.Web.Controllers
{
    public class SecurityController : Controller
    {
        public static string GenerateSHA256String(string inputString)
        {
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }
        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        private readonly MovieRatingContext _context;

        public SecurityController(MovieRatingContext context)
        {
            _context = context;
        }
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginInputModel inputModel)
        {

            if (!ModelState.IsValid)
            {
                return View(inputModel);
            }
            bool authentic = await IsAuthenticAsync(inputModel.Username, inputModel.Password);
            if (!authentic)
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return View(inputModel);
            }

            // create claims
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, inputModel.Username),
            };

            // create identity
            ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

            // create principal
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            // sign-in
            await HttpContext.SignInAsync(
                    scheme: "MovieDatabase",
                    principal: principal);

            return RedirectToAction("Index", "Home");
        }
        public IActionResult Registration()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Registration(RegistrationInputModel inputModel)
        {
            if (_context.User.Any(u => u.Username == inputModel.Username))
                ModelState.AddModelError("Username ", "User name already exists. Please enter a different user name.");

            if (!ModelState.IsValid)
            {
                return View(inputModel);
            }
            var passwordHash = GenerateSHA256String(inputModel.Password);
            _context.User.Add(new User { Username = inputModel.Username, Password = passwordHash, Email = inputModel.Email, Name = inputModel.Name});
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(
                    scheme: "MovieDatabase");

            return RedirectToAction("Login");
        }

        public IActionResult Access()
        {
            return View();
        }

        private async Task<bool> IsAuthenticAsync(string username, string password)
        {
            var user = await _context.User.SingleOrDefaultAsync(u => u.Username == username);
            if (user != null && user.Password == GenerateSHA256String(password))
            {
                return true;
            }
            return false;
        }
    }
}
