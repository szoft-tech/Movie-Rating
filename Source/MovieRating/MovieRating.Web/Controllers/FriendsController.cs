﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieRating.DataAccess.Models.DB;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace MovieRating.Web.Controllers
{
    [Authorize]
    public class FriendsController : Controller
    {
        private readonly MovieRatingContext _context;

        public FriendsController(MovieRatingContext context)
        {
            _context = context;
        }

        // GET: Friends
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var currentUser = await GetCurrentUser();
            ViewData["currentUser"] = currentUser;

            var friends = _context.Friendship.Include(f => f.FriendUser).Include(f => f.User)
                .Where(f => f.UserId == currentUser.UserId).ToList();

            friends.AddRange(_context.Friendship.Include(f => f.FriendUser).Include(f => f.User)
                .Where(f => f.FriendId == currentUser.UserId));

            var otherUsers = await _context.User.Where(u => u.UserId != currentUser.UserId).Take(15).ToListAsync();

            return View(Tuple.Create(friends, otherUsers));
        }

        // GET: Friends/SendRequest/5
        [HttpGet]
        public async Task<IActionResult> SendRequest(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Friendship request = new Friendship
            {
                UserId = (await GetCurrentUser()).UserId,
                FriendId = id ?? 0,
                Status = 0
            };

            if (!_context.Friendship.Any(f => f.UserId == request.UserId && f.FriendId == id))
            { 
                _context.Friendship.Add(request);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        // GET: Friends/AcceptRequest/5
        [HttpGet]
        public async Task<IActionResult> AcceptRequest(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User currentUser = await GetCurrentUser();

            Friendship request = await _context.Friendship
                .FirstOrDefaultAsync(f => f.UserId == id && f.FriendId == currentUser.UserId && f.Status == 0);

            if (request != null)
            {
                request.Status = 1;
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        // GET: Friends/CancelRequest/5
        [HttpGet]
        public async Task<IActionResult> CancelRequest(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User currentUser = await GetCurrentUser();

            Friendship request = await _context.Friendship
                .FirstOrDefaultAsync(f => f.FriendId == id && f.UserId == currentUser.UserId && f.Status == 0);

            if (request != null)
            {
                _context.Friendship.Remove(request);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        // GET: Friends/Unfriend/5
        [HttpGet]
        public async Task<IActionResult> Unfriend(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User currentUser = await GetCurrentUser();

            Friendship request = await _context.Friendship
                .FirstOrDefaultAsync(f => f.Status == 1 && 
                    (f.FriendId == id && f.UserId == currentUser.UserId || f.UserId == id && f.FriendId == currentUser.UserId));

            if (request != null)
            {
                _context.Friendship.Remove(request);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> FilterUser(string filter)
        {
            if (String.IsNullOrEmpty(filter))
            {
                return RedirectToAction(nameof(Index));
            }

            var currentUser = await GetCurrentUser();
            ViewData["currentUser"] = currentUser;

            List<Friendship> friends = _context.Friendship.Include(f => f.FriendUser).Include(f => f.User)
                .Where(f => f.UserId == currentUser.UserId).ToList();

            friends.AddRange(_context.Friendship.Include(f => f.FriendUser).Include(f => f.User)
                .Where(f => f.FriendId == currentUser.UserId));

            List<User> filteredUsers = 
                await _context.User.Where(u => u.UserId != currentUser.UserId &&
                                              (u.Username.Contains(filter) ||
                                               u.Email.Contains(filter) ||
                                               u.Name.Contains(filter))).ToListAsync();

            return View(nameof(Index), Tuple.Create(friends, filteredUsers));
        }

        virtual protected async Task<User> GetCurrentUser()
        {
            User currentUser = null;
            foreach (Claim claim in User.Claims.ToList())
            {
                if (claim.Type == ClaimTypes.Name)
                {
                    currentUser = await _context.User.FirstOrDefaultAsync(u => u.Username.Equals(claim.Value));
                }
            }
            return currentUser ?? new User();
        }
    }
}
