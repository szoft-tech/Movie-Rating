#!/bin/bash
docker build -t registry.gitlab.com/szoft-tech/movie-rating:latest .
docker push registry.gitlab.com/szoft-tech/movie-rating:latest
echo "Release done. Docker image with latest tag has been uploaded to the GitLab registry."
