﻿CREATE TABLE [dbo].[userlist]
(
	[userlist_id] INT NOT NULL IDENTITY (1, 1), 
	[user_id] INT NOT NULL,
    [name] NVARCHAR(50) NOT NULL,
	[list_public] BIT DEFAULT 0
	CONSTRAINT [pk_userlist] PRIMARY KEY CLUSTERED ([userlist_id] ASC),
	CONSTRAINT [fk_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user] ([user_id]),
)