﻿CREATE TABLE [dbo].[movie_list]
(
	[list_element_id] INT NOT NULL IDENTITY (1, 1),
	[userlist_id] INT NOT NULL,
	[movie_id] INT NOT NULL
	CONSTRAINT [pk_movie_list] PRIMARY KEY CLUSTERED ([list_element_id] ASC),
	CONSTRAINT [fk_list_userlist] FOREIGN KEY ([userlist_id]) REFERENCES [dbo].[userlist] ([userlist_id]),
	CONSTRAINT [fk_list_movie] FOREIGN KEY ([movie_id]) REFERENCES [dbo].[movie] ([movie_id])
)