﻿CREATE TABLE [dbo].[movie]
(
	[movie_id] INT NOT NULL IDENTITY (1, 1), 
    [movie_api_id] INT NOT NULL, 
	[movie_type_id] INT NOT NULL,
    [name] NVARCHAR(50) NOT NULL, 
    [year] SMALLINT NOT NULL, 
	[imdb_id] NVARCHAR(20) NULL,
    [added_date] DATETIME NULL DEFAULT (getdate())
	CONSTRAINT [pk_movie] PRIMARY KEY CLUSTERED ([movie_id] ASC),
	CONSTRAINT [fk_movie_movie_type] FOREIGN KEY ([movie_type_id]) REFERENCES [dbo].[movie_type] ([movie_type_id])
)
