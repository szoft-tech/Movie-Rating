﻿CREATE TABLE [dbo].[usergroup]
(
	[usergroup_id] INT NOT NULL IDENTITY (1, 1), 
	[group_id] INT NOT NULL,
    [user_id] INT NOT NULL, 
    [level] INT NOT NULL, 
	[joined_date] DATETIME NULL DEFAULT (getdate())
	CONSTRAINT [pk_usergroup] PRIMARY KEY CLUSTERED ([usergroup_id] ASC),
	CONSTRAINT [fk_usergroup_group] FOREIGN KEY ([group_id]) REFERENCES [dbo].[group] ([group_id]),
	CONSTRAINT [fk_usergroup_user] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user] ([user_id])
)
