﻿CREATE TABLE [dbo].[rating]
(
	[rating_id] INT NOT NULL IDENTITY (1, 1), 
    [user_id] INT NOT NULL, 
	[movie_id] INT NOT NULL,
    [score] INT NOT NULL, 
    [description] NVARCHAR(500) NULL, 
    [rated_date] DATETIME NULL DEFAULT (getdate())
	CONSTRAINT [pk_rating] PRIMARY KEY CLUSTERED ([rating_id] ASC),
	CONSTRAINT [fk_rating_user] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user] ([user_id]),
	CONSTRAINT [fk_rating_movie] FOREIGN KEY ([movie_id]) REFERENCES [dbo].[movie] ([movie_id])
)
