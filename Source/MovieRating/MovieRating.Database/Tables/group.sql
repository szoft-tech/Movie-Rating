﻿CREATE TABLE [dbo].[group]
(
	[group_id] INT NOT NULL IDENTITY (1, 1), 
    [name] NVARCHAR(200) NOT NULL, 
    [description] NVARCHAR(200) NULL, 
    [created_date] DATETIME NULL DEFAULT (getdate())
	CONSTRAINT [pk_group] PRIMARY KEY CLUSTERED ([group_id] ASC)
)
