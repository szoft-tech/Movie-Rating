﻿CREATE TABLE [dbo].[friendship]
(
	[friendship_id] INT NOT NULL IDENTITY (1, 1),
	[user_id] INT NOT NULL, 
	[friend_id] INT NOT NULL, 
    [status] INT NOT NULL, 
	[friended_date] DATETIME NULL DEFAULT (getdate())
	CONSTRAINT [pk_friendship] PRIMARY KEY CLUSTERED ([friendship_id] ASC),
	CONSTRAINT [fk_friendship_user_1] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user] ([user_id]),
	CONSTRAINT [fk_friendship_user_2] FOREIGN KEY ([friend_id]) REFERENCES [dbo].[user] ([user_id]),
	CONSTRAINT [uk_friendship] UNIQUE NONCLUSTERED ([user_id], [friend_id])
)
