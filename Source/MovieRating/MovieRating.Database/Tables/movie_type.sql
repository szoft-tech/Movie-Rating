﻿CREATE TABLE [dbo].[movie_type]
(
	[movie_type_id] INT NOT NULL IDENTITY (1, 1), 
    [name] NVARCHAR(50) NOT NULL, 
	CONSTRAINT [pk_movie_type] PRIMARY KEY CLUSTERED ([movie_type_id] ASC),
)
