﻿CREATE TABLE [dbo].[user]
(
	[user_id] INT NOT NULL IDENTITY (1, 1), 
    [username] NVARCHAR(20) NOT NULL, 
    [password] NVARCHAR(128) NOT NULL, 
    [email] NVARCHAR(50) NULL, 
    [name] NVARCHAR(30) NULL, 
    [joined_date] DATETIME NULL DEFAULT (getdate())
	CONSTRAINT [pk_user] PRIMARY KEY CLUSTERED ([user_id] ASC),
)
