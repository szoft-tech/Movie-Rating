#!/bin/bash
echo "Removing docker container and image if already exists..."
docker stop movie-rating-web
docker rm movie-rating-web
docker rmi registry.gitlab.com/szoft-tech/movie-rating:latest
echo "Starting movie-rating-web container..."
docker run --name movie_rating -d -p 8088:80 registry.gitlab.com/szoft-tech/movie-rating:latest
echo "------------------------------------------------------------------"
echo "Docker container has started. Webpage URL: http://localhost:8088"
echo "To stop the container, use 'docker stop movie-rating-web' command."
