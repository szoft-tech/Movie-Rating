#!/bin/bash
echo "Building registry.gitlab.com/szoft-tech/movie-rating/web:development image..."
docker build -t registry.gitlab.com/szoft-tech/movie-rating/web:development .
echo "Removing docker container if already exists..."
docker stop movie-rating-web
docker rm movie-rating-web
echo "Starting movie-rating-web container..."
docker run --name movie-rating-web -d -p 8088:80 registry.gitlab.com/szoft-tech/movie-rating/web:development
echo "------------------------------------------------------------------"
echo "Docker container has started. Webpage URL: http://localhost:8088"
echo "To stop the container, use 'docker stop movie-rating-web' command."
